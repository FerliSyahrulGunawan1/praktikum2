import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:any={};
  
  constructor(
  public router: Router,
  public api:ApiService
  ) { }

  ngOnInit(): void {
  }

  loading:boolean=true;
  register(user:any)
  {
    this.loading=true;
    this.api.register(user.email, user.password).subscribe(result=>{
      this.loading=false;
      // console.log(result);
      alert('Register berhasil')
      this.router.navigate(['auth/login']);
    },error=>{
      this.loading=false;
      alert('ada masalah...');
    });
  }

  }
