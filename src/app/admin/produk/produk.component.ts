import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as FileSaver from 'file-saver';
import { ApiService } from 'src/app/api.service';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';
import { ProdukDetailComponent } from '../produk-detail/produk-detail.component';


@Component({
  selector: 'app-produk',
  templateUrl: './produk.component.html',
  styleUrls: ['./produk.component.css']
})


export class ProdukComponent implements OnInit {
  title:any;
      book:any={};
      books:any=[];
  // dialog: any;
 constructor(
  public router: Router,
  public dialog: MatDialog,
  public api:ApiService
 ) {
      this.title='Produk';
      this.getBooks();
      // console.getBooks();
    }
  ngOnInit(): void {
    this.title='product';
  // this.getBooks();
  }
//2. Membuat fungsi
loading:boolean=false;
  getBooks()
  {
  this.loading=true;
  this.api.get('bookswithauth').subscribe(result=>{
    this.books=result;
    this.loading=false;
  },error=>{
    this.loading=false;
    alert('Tidak dapat mengambil data');
  });

  }
  
  productDetail(data:any ,idx:any)
  {
    let dialog=this.dialog.open(ProdukDetailComponent, {
      width:'400px',
      data:data
    });
    dialog.afterClosed().subscribe((res:any)=>{
      if(res)
      {
        //jika idx=-1 (penambahan data baru) maka tambahkan data
        if(idx==-1)this.books.push(res);      
        //jika tidak maka perbarui data  
        else this.books[idx]=res; 
      }
    })
}

  loadingDelete:any={};
  deleteProduct(idx:any)
 {
   var conf=confirm('Delete item?');
   if(conf)
   this.loadingDelete[idx]=true;
   this.api.delete('books/'+this.books[idx].id).subscribe(result=>{
    this.books.splice(idx,1);
    this.loadingDelete[idx]=false;

  },error=>{
    alert('Tidak dapat menghapus data');
    this.loadingDelete[idx]=false;

  });

 }

upload(data:any, idx:any)
{
 let dialog=this.dialog.open(FileUploaderComponent, {
   width:'400px',
   data:data
 });
 dialog.afterClosed().subscribe(res=>{
   return;
 })
}

downloadFile(data:any)
{
  FileSaver.saveAs('http://api.sunhouse.co.id/bookstore//'+data.url);
}

}


