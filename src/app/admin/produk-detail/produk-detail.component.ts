import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-produk-detail',
  templateUrl: './produk-detail.component.html',
  styleUrls: ['./produk-detail.component.css']
})
export class ProdukDetailComponent implements OnInit {



  constructor(
    public dialogRef:MatDialogRef<ProdukDetailComponent>,
   @Inject(MAT_DIALOG_DATA) public data: any,
   public api:ApiService

  ) { }

  ngOnInit(): void {
  }
loading:boolean=false;  
saveData()
    {
      this.loading=true;
        if(this.data.id == undefined)
        {
            this.api.post('books',this.data).subscribe(result=>{
            this.dialogRef.close(result);
            this.loading=false;
        },error=>{
          //kondisi jika terjadi masalah pengiriman pada pengiriman data
          alert(error);
          this.loading=false;
        })
  
        }
        else{
      
            this.api.put('books/'+this.data.id,this.data).subscribe((result)=>{
              console.log(result);
              this.loading=false;
              this.dialogRef.close(result);
              location.reload();
      
          },error=>{
            //kondisi jika terjadi masalah pengiriman pada pengiriman data
            alert(error);
            this.loading=false;

          })
    }

  }
}
