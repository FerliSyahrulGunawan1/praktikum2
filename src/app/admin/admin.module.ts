import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {RouterModule, Routes } from '@angular/router';
import { MaterilaDesain } from '../material/material';
import { ProdukComponent } from './produk/produk.component';
import { ProdukDetailComponent } from './produk-detail/produk-detail.component';
import { FormsModule } from '@angular/forms';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';

const routes: Routes = [
  {
    path:'',
    component:AdminComponent,
    children:[
      {
        path:'dashboard',
        component: DashboardComponent
      },
      {
        path:'produk',
        component:ProdukComponent
      }
    ]
  }

]

@NgModule({
  declarations: [AdminComponent, DashboardComponent, ProdukComponent, ProdukDetailComponent, FileUploaderComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterilaDesain,
    FormsModule
  ]
})
export class AdminModule { }
