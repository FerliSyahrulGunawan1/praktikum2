import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  mode:string='side';
  constructor(
    public api:ApiService,
    public router:Router
  ) { }

  ngOnInit(): void {
    this.checkLogin();
  }

  checkLogin()
  {
    this.api.get('bookswithauth/status').subscribe(res=>{
      return;
    },err=>{
      this.router.navigate(['/login']);
    })
  }

  logout()
  {
    let conf=confirm('Keluar Aplikasi?');
    if(conf)
    {
      localStorage.removeItem('appToken');
      window.location.reload();
    }
  }
}
